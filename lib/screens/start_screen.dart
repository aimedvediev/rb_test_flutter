import 'package:flutter/material.dart';

import 'package:flutter_rb_test/models/sleep_record.dart';
import 'package:flutter_rb_test/providers/sleeping_records_provider.dart';

import 'package:intl/intl.dart';

import 'package:flutter_rb_test/screens/add_slepping_record_screen.dart';

import 'package:flutter_rb_test/widgets/button_default.dart';

import 'package:flutter_rb_test/widgets/start_screen/sleep_record_item.dart';

import 'package:provider/provider.dart';

class StartScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var now = new DateTime.now();
    var time = DateFormat('EEEE, d MMM y').format(now);

    double screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Color(0xFFFAFAFA),
      appBar: AppBar(
        backgroundColor: Colors.amber,
        title: Text(
          'Sleep Tracker',
        ),
        centerTitle: false,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Center(
                //https://www.iconfinder.com/icons/2995009/giallo_luna_moon_night_satellite_space_yellow_ico
                child: Image.network(
                  'https://cdn3.iconfinder.com/data/icons/tiny-weather-1/512/moon-512.png',
                  height: 60,
                  width: 60,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 40, right: 40, top: 10),
              child: Center(
                child: Text(
                  'Get to know your baby\'s sleep patterns and keep track of how much sleep they are getting here.',
                  style: TextStyle(
                    color: Colors.black54,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20, top: 15),
              child: ButtonDefault(
                text: 'Add new sleeping record',
                onSelect: () {
                  Navigator.of(context)
                      .pushNamed(AddSleepingRecordScreen.routeName);
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 80, left: 20),
              child: Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  time,
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.black54,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 20, right: 20, top: 25, bottom: 30),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(25),
                child: Container(
                  color: Colors.white,
                  child: Column(
                    children: <Widget>[
                      ...Provider.of<SleepingRecordsProvider>(context)
                          .items
                          .map(
                        (item) {
                          return SleepRecordItem(
                            dayTime: item.dayTime,
                            sleepDuration: item.sleepDuration,
                            sleepType: item.sleepType,
                            time: item.time,
                          );
                        },
                      ).toList(),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
