import 'package:flutter/material.dart';
import 'package:flutter_rb_test/providers/sleeping_records_provider.dart';
import 'package:intl/intl.dart';

import 'package:flutter_rb_test/models/sleep_record.dart';

import 'package:flutter_rb_test/widgets/add_sleeping_records_screen/custom_filed_button.dart';
import 'package:flutter_rb_test/widgets/add_sleeping_records_screen/dialog_sleeptype.dart';

import 'package:flutter_rb_test/widgets/custom_picker.dart';
import 'package:flutter_rb_test/widgets/button_default.dart';

import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:provider/provider.dart';

class AddSleepingRecordScreen extends StatefulWidget {
  static const routeName = '/add_sleeping_record_screen';

  @override
  _AddSleepingRecordScreenState createState() =>
      _AddSleepingRecordScreenState();
}

class _AddSleepingRecordScreenState extends State<AddSleepingRecordScreen> {
  SLEEP_TYPE _sleepType = SLEEP_TYPE.NOPE;
  String _sleepDuration = '-';

  var _editedSleepingRecord = SleepRecord(
    time: '',
    dayTime: '',
    sleepType: SLEEP_TYPE.NOPE,
    sleepDuration: '',
  );

  @override
  Widget build(BuildContext context) {
    var now = new DateTime.now();
    var time = DateFormat('d MMMM y, HH:mm').format(now);

    String hours;
    String minutes;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.amber,
        title: Text('Sleeping tracker'),
        centerTitle: false,
      ),
      body: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(30),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Image.network(
                  'https://images.unsplash.com/photo-1539099362493-b7ca4038c646?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1907&q=80'),
            ),
          ),
          CustomField(
            text: 'Date and time',
            icon: Icons.calendar_today,
            subText: time,
            onPressed: () {
              setState(() {
                now = new DateTime.now();
              });
            },
          ),
          CustomField(
            text: 'Sleep type',
            icon: Icons.brightness_3,
            subText: _sleepType == SLEEP_TYPE.NIGHT
                ? 'Night’s Sleep'
                : _sleepType == SLEEP_TYPE.NAP ? 'Nap' : 'Night, nap, etc',
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return DialogSleepType(
                      setType: (type) {
                        setState(() {
                          _editedSleepingRecord = SleepRecord(
                            time: _editedSleepingRecord.time,
                            dayTime: _editedSleepingRecord.dayTime,
                            sleepType: type,
                            sleepDuration: _editedSleepingRecord.sleepDuration,
                          );
                          _sleepType = type;
                        });
                      },
                    );
                  });
            },
          ),
          CustomField(
              text: 'Sleep Duration',
              icon: Icons.access_time,
              subText: _sleepDuration,
              onPressed: () {
                DatePicker.showPicker(context, showTitleActions: true,
                    onConfirm: (date) {
                  hours = date.hour.toString();
                  minutes = date.minute.toString();
                  setState(() {
                    if (hours == '0' && minutes == '0') {
                      _sleepDuration = '-';
                    } else {
                      _sleepDuration =
                          '${hours == '0' ? '' : hours == '1' ? '$hours hour' : '$hours hours'} ${minutes == '0' ? '' : minutes == '1' ? '$minutes minut' : '$minutes minutes'}';
                    }
                    _editedSleepingRecord = SleepRecord(
                      time: DateFormat('KK:mm').format(now),
                      dayTime: DateFormat('a').format(now),
                      sleepType: _editedSleepingRecord.sleepType,
                      sleepDuration: _sleepDuration,
                    );
                  });
                },
                    pickerModel: CustomPicker(
                        currentTime: DateTime(2020, 04, 04, 04, 30, 00)),
                    locale: LocaleType.en);
              }),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, top: 45),
            child: ButtonDefault(
              text: 'Save',
              onSelect: () {
                if (_editedSleepingRecord.sleepType == SLEEP_TYPE.NOPE ||
                    _sleepDuration == '-') {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: Text('Error'),
                          content: Text(
                              'Please select Sleep type and Sleep Duration'),
                          actions: <Widget>[
                            FlatButton(
                              child: Text('OK'),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            )
                          ],
                        );
                      });
                } else {
                  Provider.of<SleepingRecordsProvider>(context, listen: false)
                      .addRecord(
                    _editedSleepingRecord,
                  );
                  Navigator.of(context).pop();
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}
