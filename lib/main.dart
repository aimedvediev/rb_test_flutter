import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:flutter_rb_test/providers/sleeping_records_provider.dart';

import 'package:flutter_rb_test/screens/add_slepping_record_screen.dart';
import 'package:flutter_rb_test/screens/start_screen.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => SleepingRecordsProvider(),
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: StartScreen(),
        routes: {
          AddSleepingRecordScreen.routeName: (ctx) => AddSleepingRecordScreen()
        },
      ),
    );
  }
}
