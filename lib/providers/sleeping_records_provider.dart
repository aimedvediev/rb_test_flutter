import 'package:flutter/material.dart';
import 'package:flutter_rb_test/models/sleep_record.dart';

import 'package:flutter_rb_test/widgets/start_screen/sleep_record_item.dart';

class SleepingRecordsProvider with ChangeNotifier {
  List<SleepRecord> _items = [];

  List<SleepRecord> get items {
    return [..._items.reversed];
  }

  void addRecord(SleepRecord record) {
    _items.add(record);
    notifyListeners();
  }
}
