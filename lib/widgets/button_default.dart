import 'package:flutter/material.dart';

class ButtonDefault extends StatefulWidget {
  final String text;
  final Function() onSelect;

  ButtonDefault({
    this.text,
    this.onSelect,
  });

  @override
  _ButtonDefaultState createState() => _ButtonDefaultState();
}

class _ButtonDefaultState extends State<ButtonDefault> {
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;

    return ClipRRect(
      borderRadius: BorderRadius.circular(25),
      child: Container(
        width: screenWidth,
        height: 45,
        child: RaisedButton(
          color: Color(0xFF0A02B4),
          child: Text(
            widget.text,
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          onPressed: () {
            setState(() {
              widget.onSelect();
            });
          },
        ),
      ),
    );
  }
}
