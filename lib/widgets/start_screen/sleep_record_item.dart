import 'package:flutter/material.dart';

import 'package:flutter_rb_test/models/sleep_record.dart';

class SleepRecordItem extends StatelessWidget {
  final String time;
  final String dayTime;
  final SLEEP_TYPE sleepType;
  final String sleepDuration;

  SleepRecordItem({
    this.time,
    this.dayTime,
    this.sleepType,
    this.sleepDuration,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                width: 80,
                padding: EdgeInsets.all(16),
                color: Color(0xFFf4f1ea),
                child: Column(
                  children: <Widget>[
                    Text(
                      time,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                        letterSpacing: 0.5,
                      ),
                    ),
                    Text(
                      dayTime,
                      style: TextStyle(
                        fontWeight: FontWeight.w300,
                        fontSize: 12,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.all(10),
                color: Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      sleepType == SLEEP_TYPE.NIGHT
                          ? 'Night’s Sleep'
                          : sleepType == SLEEP_TYPE.NAP
                              ? 'Nap'
                              : 'Night, nap, etc',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: Color(0xFF0A02B4),
                          letterSpacing: 0.5),
                    ),
                    Text(sleepDuration),
                  ],
                ),
              ),
            ],
          ),
          Divider(
            height: 0,
          ),
        ],
      ),
    );
  }
}
