import 'package:flutter/material.dart';

class CustomField extends StatefulWidget {
  final String text;
  final IconData icon;
  final String subText;
  final Function() onPressed;

  CustomField({
    this.text,
    this.icon,
    this.subText,
    this.onPressed,
  });

  @override
  _CustomFieldState createState() => _CustomFieldState();
}

class _CustomFieldState extends State<CustomField> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: FlatButton(
        onPressed: () {
          setState(() {
            widget.onPressed();
          });
        },
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(15),
                  child: Icon(
                    widget.icon,
                    color: Color(0xFF0A02B4),
                    size: 20,
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      widget.text,
                      style: TextStyle(
                        color: Color(0xFF0A02B4),
                        fontSize: 13,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      widget.subText,
                      style: TextStyle(
                          fontSize: 15,
                          color: widget.subText != '-' &&
                                  widget.subText != 'Night, nap, etc'
                              ? Colors.black
                              : Colors.black26,
                          fontWeight: FontWeight.normal),
                    ),
                  ],
                )
              ],
            ),
            Divider(
              height: 0,
              color: Colors.black38,
            ),
          ],
        ),
      ),
    );
  }
}
