import 'package:flutter/material.dart';

import 'package:flutter_rb_test/models/sleep_record.dart';

class DialogSleepType extends StatefulWidget {
  Function(SLEEP_TYPE) setType;

  DialogSleepType({
    this.setType,
  });

  @override
  _DialogSleepTypeState createState() => _DialogSleepTypeState();
}

class _DialogSleepTypeState extends State<DialogSleepType> {
  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      title: Text(
        'Please select a sleep type: ',
        style: TextStyle(fontSize: 14),
      ),
      children: <Widget>[
        FlatButton(
            child: Text('Night’s Sleep'),
            onPressed: () {
              setState(() {
                widget.setType(SLEEP_TYPE.NIGHT);
                Navigator.pop(context);
              });
            }),
        FlatButton(
            child: Text('Nap'),
            onPressed: () {
              setState(() {
                widget.setType(SLEEP_TYPE.NAP);
                Navigator.pop(context);
              });
            }),
      ],
    );
  }
}
