enum SLEEP_TYPE { NIGHT, NAP, NOPE }

class SleepRecord {
  final String time;
  final String dayTime;
  final SLEEP_TYPE sleepType;
  final String sleepDuration;

  SleepRecord({
    this.time,
    this.dayTime,
    this.sleepType,
    this.sleepDuration,
  });
}